import { StatusBar } from "expo-status-bar";
import React, { useState } from "react";
import { AppLoading } from "expo";
import { Provider } from "react-redux";

import { RootNav } from "./navigation/RootNav";
import { loadFonts } from "./styles/fonts";
import store from "./store";

export default function App() {
  const [loaded, setLoaded] = useState(false);
  if (!loaded) {
    return (
      <AppLoading
        startAsync={loadFonts}
        onFinish={() => setLoaded(true)}
        onError={() => console.log("Error occured when load the fonts")}
      />
    );
  } else {
    return (
      <Provider store={store}>
        <RootNav />
      </Provider>
    );
  }
}
