import React from "react";
import { StyleSheet, View, StatusBar } from "react-native";

export const Container = ({ children }) => {
  return (
    <View style={styles.container}>
      <StatusBar
        translucent={true}
        barStyle="dark-content"
        backgroundColor="white"
      />
      <View style={styles.content}>{children}</View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 20,
    paddingTop: 60,
    backgroundColor: "#FFF",
  },
  content: {
    flex: 1,
  },
});
