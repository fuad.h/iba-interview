import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import { HomeScreen } from "../screens/HomeScreen";
import { Details } from "../screens/Details";

const { Navigator, Screen } = createStackNavigator();

export const AppStack = () => (
  <Navigator>
    <Screen
      name="Home"
      options={{ headerShown: false }}
      component={HomeScreen}
    />
    <Screen
      name="Details"
      component={Details}
      options={({ route }) => ({
        headerTitle: route?.params?.name,
        headerTitleAlign: "center",
        headerTintColor: "red",
        headerStyle: {
          elevation: 0,
        },
      })}
    />
  </Navigator>
);
