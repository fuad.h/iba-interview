import React from "react";
import { StyleSheet, View, TouchableOpacity, Image } from "react-native";
import { DefText } from "./Text";
import { ICONS } from "../styles/icons";

export const DefButton = ({ img, text }) => {
  return (
    <TouchableOpacity style={styles.wrapper}>
      <Image style={styles.img} source={ICONS[img]} />
      <DefText>{text}</DefText>
    </TouchableOpacity>
  );
};
const styles = StyleSheet.create({
  wrapper: {
    width: 100,
    height: 30,
    backgroundColor: "grey",
    borderRadius: 10,
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "center",
  },
  img: {
    height: 20,
    width: 20,
  },
});
