export function getFormmatedTime(time) {
  const date = new Date(time);
  const day = date.getDay();
  const year = date.getFullYear();
  const month = date.getMonth();

  const months = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];

  return `${day} ${months[month]} ${year}`;
}
