import { createStore, combineReducers } from "redux";
import {
  MODULE_NAME as paymentModuleName,
  reducer as paymentReducer,
} from "./paymentDetails";

const rootReducer = combineReducers({
  [paymentModuleName]: paymentReducer,
});

const store = createStore(rootReducer);

export default store;
