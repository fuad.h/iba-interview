//ACTION TYPE
const PAYMENTS = "PAYMENTS";

//SELECTORTS
export const MODULE_NAME = "payments";
export const selectPaymentLists = (state) => state[MODULE_NAME].list;
export const selectPaymentObject = (state, id) =>
  state[MODULE_NAME].list.find((item) => item.id === id);

//REDUCER
const initialState = {
  list: [
    {
      id: "1",
      img:
        "https://images.unsplash.com/photo-1519764622345-23439dd774f7?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60",
      name: "Fuad",
      amount: "72,567$",
      details: {
        payment_detail: Date.now(),
        type: "Debt",
        pay_with: "Credit Account",
      },
    },
    {
      id: "2",
      img:
        "https://images.unsplash.com/photo-1559386484-97dfc0e15539?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60",
      name: "Ferid",
      amount: "75,567$",
      details: {
        payment_detail: Date.now(),
        type: "Debt",
        pay_with: "Credit Account",
      },
    },
    {
      id: "3",
      img:
        "https://images.unsplash.com/photo-1502307100811-6bdc0981a85b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60",
      name: "Jhon",
      amount: "77,567$",
      details: {
        payment_detail: Date.now(),
        type: "Debt",
        pay_with: "Credit Account",
      },
    },
    {
      id: "4",
      img:
        "https://images.unsplash.com/photo-1529626455594-4ff0802cfb7e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60",
      name: "Julie",
      amount: "75,567$",
      details: {
        payment_detail: Date.now(),
        type: "Debt",
        pay_with: "Credit Account",
      },
    },
    {
      id: "5",
      img:
        "https://images.unsplash.com/photo-1577394745831-5f5351823199?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60",
      name: "Tom",
      amount: "75,567$",
      details: {
        payment_detail: Date.now(),
        type: "Debt",
        pay_with: "Credit Account",
      },
    },
    {
      id: "6",
      img:
        "https://images.unsplash.com/photo-1534260748473-e1c629d04bb0?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60",
      name: "Jack",
      amount: "75,567$",
      details: {
        payment_detail: Date.now(),
        type: "Debt",
        pay_with: "Credit Account",
      },
    },
    {
      id: "7",
      img:
        "https://images.unsplash.com/photo-1551022372-0bdac482b9d6?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60",
      name: "Sezar",
      amount: "75,567$",
      details: {
        payment_detail: Date.now(),
        type: "Debt",
        pay_with: "Credit Account",
      },
    },
    {
      id: "8",
      img:
        "https://images.unsplash.com/photo-1525802915797-2c49d723885f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60",
      name: "Caprio",
      amount: "75,567$",
      details: {
        payment_detail: Date.now(),
        type: "Debt",
        pay_with: "Credit Account",
      },
    },
    {
      id: "9",
      img:
        "https://images.unsplash.com/photo-1525802915797-2c49d723885f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60",
      name: "Caprio",
      amount: "75,567$",
      details: {
        payment_detail: Date.now(),
        type: "Debt",
        pay_with: "Credit Account",
      },
    },
  ],
};

export function reducer(state = initialState, { type, payload }) {
  switch (type) {
    default:
      return state;
  }
}
