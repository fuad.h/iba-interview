import search from "../assets/icons/search.png";
import pay from "../assets/icons/pay.png";
import dollar from "../assets/icons/dollar.png";

export const ICONS = Object.freeze({
  search,
  pay,
  dollar,
});
