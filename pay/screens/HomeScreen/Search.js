import React from "react";
import {
  StyleSheet,
  View,
  TextInput,
  TouchableOpacity,
  Image,
} from "react-native";

import { FONT_FAMILIES } from "../../styles/fonts";
import { ICONS } from "../../styles/icons";

export const Search = () => {
  return (
    <View style={styles.container}>
      <TextInput placeholder="search" style={styles.text} />
      <View style={styles.iconWrapper}>
        <Image style={styles.icon} source={ICONS.search} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    borderWidth: 1,
    borderColor: "#eee",
    borderRadius: 7,
    height: 36,
    textAlign: "center",
    justifyContent: "center",
    paddingHorizontal: 15,
    flexDirection: "row",
    justifyContent: "space-between",
  },
  text: {
    fontFamily: FONT_FAMILIES.regular,
    width: "90%",
  },
  iconWrapper: {
    width: "10%",
    height: 36,
    justifyContent: "center",
    alignItems: "center",
  },
  icon: {
    width: 20,
    height: 20,
  },
});
