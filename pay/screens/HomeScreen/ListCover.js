import React from "react";
import { StyleSheet, View, Image, TouchableOpacity } from "react-native";

import { DefText } from "../../components/Text";

export const ListCover = ({ name, img, amount, onPress }) => {
  return (
    <TouchableOpacity style={styles.container} onPress={onPress}>
      <View style={styles.wrapper}>
        <View style={styles.imageWrapper}>
          <Image style={styles.img} source={{ uri: img }} />
        </View>
        <DefText weight="semi" style={styles.name}>
          {name}
        </DefText>
      </View>
      <DefText weight="medium" style={styles.amount}>
        {amount}
      </DefText>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginVertical: 10,
    marginRight: 20,
  },
  wrapper: {
    width: "30%",
    flexDirection: "row",
    justifyContent: "space-between",
  },
  img: {
    width: 30,
    height: 30,
  },
  imageWrapper: {
    height: 30,
    width: 30,
    borderRadius: 20,
    overflow: "hidden",
  },
  name: {
    fontSize: 15,
  },
  amount: {
    opacity: 0.4,
  },
});
