import React from "react";
import { StyleSheet, View } from "react-native";

import { ProgressCircle } from "react-native-svg-charts";
import { DefText } from "../../components/Text";

export const Progress = ({ color, title, percentage }) => {
  return (
    <View style={styles.container}>
      <DefText style={styles.title}>{title}</DefText>
      <DefText style={styles.text}>{percentage}%</DefText>
      <ProgressCircle
        style={{ height: 100 }}
        progress={+percentage / 100}
        progressColor={color}
        strokeWidth={5}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 130,
    width: 100,
  },
  title: {
    textAlign: "center",
    marginBottom: 10,
    color: "#eee",
  },
  text: {
    position: "absolute",
    top: "55%",
    left: "35%",
  },
});
