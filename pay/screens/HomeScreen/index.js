import React from "react";
import { StyleSheet, View, FlatList } from "react-native";

import { Performance } from "./Performance";
import { Container } from "../../commons/Container";
import { DefText } from "../../components/Text";
import { Search } from "./Search";
import { ListCover } from "./ListCover";
import { connect } from "react-redux";
import { selectPaymentLists } from "../../store/paymentDetails";

const mapStateToProps = (state) => ({
  lists: selectPaymentLists(state),
});

export const HomeScreen = connect(mapStateToProps)(({ navigation, lists }) => {
  const pressHandler = (id, name) => {
    navigation.navigate("Details", { id, name });
  };
  return (
    <Container>
      <DefText weight="bold" style={styles.title}>
        Transactions
      </DefText>
      <Search />
      <DefText weight="semi" style={styles.secondaryTitle}>
        Performance
      </DefText>
      <Performance />
      <DefText weight="semi" style={styles.thirdTitle}>
        Transactions
      </DefText>
      <FlatList
        data={lists}
        renderItem={({ item }) => (
          <ListCover
            name={item.name}
            img={item.img}
            amount={item.amount}
            onPress={() => pressHandler(item.id, item.name)}
          />
        )}
      />
    </Container>
  );
});

const styles = StyleSheet.create({
  title: {
    fontSize: 25,
    marginBottom: 20,
  },
  secondaryTitle: {
    marginVertical: 15,
    fontSize: 18,
  },
  thirdTitle: {
    fontSize: 14,
    marginTop: 10,
    marginBottom: 20,
  },
});
