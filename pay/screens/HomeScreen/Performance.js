import React from "react";
import { StyleSheet, View, Text } from "react-native";

import { Progress } from "./Progress";

const ProgressDetails = [
  { title: "Current week", color: "green", percentage: "25", id: 1 },
  { title: "Last week", color: "red", percentage: "50", id: 2 },
  { title: "Last Month", color: "blue", percentage: "75", id: 3 },
];

export const Performance = () => {
  return (
    <View style={styles.container}>
      {ProgressDetails.map((item) => (
        //    TODO GETWIDTH BY SCREEN SIZE
        <Progress
          key={item.id}
          title={item.title}
          color={item.color}
          percentage={item.percentage}
        />
      ))}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
});
