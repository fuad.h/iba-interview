import React from "react";
import { StyleSheet, View, Text } from "react-native";
import { connect } from "react-redux";

import { selectPaymentObject } from "../../store/paymentDetails";
import { Container } from "../../commons/Container";
import { DefText } from "../../components/Text";
import { DefButton } from "../../components/Button";
import { getFormmatedTime } from "../../utils/getFormattedTime";



const mapStateToProps = (state, { route }) => ({
  detail: selectPaymentObject(state, route.params.id),
});

export const Details = connect(mapStateToProps)(({ route, detail }) => {
  return (
    <Container>
      <View style={styles.amountWrapper}>
        <DefText style={styles.amount} weight="medium">
          {detail.amount}
        </DefText>
      </View>
      <View style={styles.buttonWrapper}>
        <DefButton img="pay" text="Card" />
        <DefButton img="dollar" text="Debt" />
      </View>
      <DefText weight="semi" style={styles.detail}>
        Transaction detail
      </DefText>

      <View style={styles.row}>
        <DefText weight="semi">Payment Detail</DefText>
        <DefText style={styles.paymentDetail}>
          {getFormmatedTime(detail.details.payment_detail)}
        </DefText>
      </View>
      <View style={styles.row}>
        <DefText weight="semi">Type</DefText>
        <DefText style={styles.paymentDetail}>{detail.details.type}</DefText>
      </View>
      <View style={styles.row}>
        <DefText weight="semi">Pay with</DefText>
        <DefText style={styles.paymentDetail}>
          {detail.details.pay_with}
        </DefText>
      </View>
    </Container>
  );
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  amountWrapper: {
    justifyContent: "center",
    alignItems: "center",
  },
  amount: {
    fontSize: 27,
  },
  buttonWrapper: {
    flexDirection: "row",
    paddingHorizontal: 40,
    justifyContent: "space-between",
    marginTop: 60,
    marginBottom: 25,
  },
  detail: {
    marginTop: 10,
    paddingBottom: 10,
    borderBottomWidth: 1,
    borderColor: "#eee",
    marginBottom: 50,
  },
  row: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginVertical: 10,
    borderBottomWidth: 1,
    borderColor: "#eee",
  },
  paymentDetail: {
    opacity: 0.4,
  },
});
